//
//  iDineApp.swift
//  iDine
//
//  Created by Marc  Matvienko on 3/3/21.
//

import SwiftUI

@main
struct iDineApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
