//
//  ItemRow.swift
//  iDine
//
//  Created by Marc  Matvienko on 3/4/21.
//

import SwiftUI

struct ItemRow: View {
    let colors: [String: Color] = [
        "D": .purple,
        "G": .black,
        "N": .red,
        "S": .blue,
        "V": .green,
    ]
    let item: MenuItem   // this is an argument to the struct and will be defined when used
    
    var body: some View {
        HStack{   // horizontal stack
            Image(item.thumbnailImage)
                .clipShape(Circle())   // clips the actual image itself
                .overlay(Circle().stroke(Color.gray, lineWidth: 1))   // gives border
            VStack(alignment: .leading){   // vertical stack
                Text(item.name)
                    .font(.headline)
                Text("$\(item.price)")
            }
            
            Spacer()   // pushes the left and right objects out as far as possible
            
            ForEach(item.restrictions, id: \.self){ restriction in
                Text(restriction)
                    .font(.caption)
                    .fontWeight(.black)
                    .padding(5)
                    .background(colors[restriction, default: .black])
                    .clipShape(Circle())
                    .foregroundColor(.white)
            }
        }
    }
}

struct ItemRow_Previews: PreviewProvider {
    static var previews: some View {
        ItemRow(item: MenuItem.example)
    }
}
